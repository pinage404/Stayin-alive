//is streaming ? 
// https://api.twitch.tv/helix/streams?user_login=krayn_live
// empty object means no

//is a channel exists ? 
// https://api.twitch.tv/kraken/channels/nonot2
// no = error 404

const fetch = require("node-fetch");
const config = require('./config.js')

const paramsDB = config['database']
const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: paramsDB['host'],
        user: paramsDB['user'],
        password: paramsDB['password'],
        database: paramsDB['database']
    }
});
const IDTWITCH = config['twitch']['CLIENT-ID']
const headers = { 'CLIENT-ID': IDTWITCH }

const tokenDiscord = config['discord']['token']
const Eris = require("eris");
var bot = new Eris(tokenDiscord);

const FIVEMINUTES = 1000 * 60 * 5;
const DISCORD = "Discord"
const UTILISE = "Utilise"
const STREAMS = "Streams"
// These are the commands the bot knows (defined below):
let knownCommands = { add, remove, togglemode, comehere, list }
let commandPrefix = "!"
const IDDIFFUSION = 'idDiffusion'

async function main() {
    console.log("lancement des vérifications");
    const listeStreams = await knex(STREAMS).select('*')
    for (let i = 0; i < listeStreams.length; i++) {
        const element = listeStreams[i];
        let diffuse
        const value = element['value']
        if (element['plateforme'] === 'twitch') {
            const resultIsStreaming = await fetch(`https://api.twitch.tv/helix/streams?user_login=${value}`, { method: 'GET', headers })
            const isStreaming = await resultIsStreaming.json()
            diffuse = isStreaming['data'].length === 0
                ? 0
                : 1
        }
        if (element['diffuse'] != diffuse) {
            const idStreams = element['idStreams']
            await knex(STREAMS).where('idStreams', '=', idStreams).update({ diffuse })
            if (diffuse === 1) {
                console.log("annonce du stream" + element['value']);
                const resultListeChannels = await knex.raw(`select Discord.idDiffusion from Discord, Utilise, Streams where Streams.idStreams = ${idStreams} and activation = 1 and Utilise.idStreams = Streams.idStreams and Utilise.idDiscord = Discord.idDiscord;`)
                const listeChannels = resultListeChannels[0]
                for (let i = 0; i < listeChannels.length; i++) {
                    const channel = listeChannels[i];
                    bot.createMessage(channel['idDiffusion'] , `Un live vient de commencer  !!! \n https://www.twitch.tv/${value}`);
                }
            }
        }
    }
}

async function initProject() {
    try {
        await knex.raw("create table if not exists Discord( `idDiscord` varchar(18) NOT NULL , `idDiffusion` varchar(18) NOT NULL, `activation` boolean NOT NULL default 0, primary key (`idDiscord`));")
        await knex.raw("create table if not exists Streams( idStreams int not null primary key auto_increment, value varchar(50) Not null, plateforme varchar(7) Not null, diffuse boolean default 0);")
        await knex.raw("create table if not exists Utilise( `idDiscord` varchar(18) NOT NULL , `idStreams` int NOT NULL, primary key (`idDiscord`, `idStreams`));")
        await knex.raw("alter table Utilise  add foreign key  (idDiscord) references Discord(idDiscord);")
        await knex.raw("alter table Utilise  add foreign key  (idStreams) references Streams(idStreams);")
    } catch (error) {
        console.log(error);
    }
}
initProject()

function isServerInMap(serverId) {
    for (var key in bot.channelGuildMap) {
        if (serverId === bot.channelGuildMap[key]) {
            return true
        }
    }
    return false
}
async function checkDB() {
    const listeServeursConnectés = await knex('Discord').select('idDiscord')
    console.log("liste des serveurs connectés " + listeServeursConnectés.length);
    console.log(listeServeursConnectés)
    for (let i = 0; i < listeServeursConnectés.length; i++) {
        const element = listeServeursConnectés[i]['idDiscord'];
        if (!isServerInMap(element)) {
            console.log(element + " n'est plus connecté au bot");
            await knex(DISCORD).del().where('idDiscord', '=', element)
        }
    }
}

setTimeout(() => {
    main()
}, 5000);
setInterval(main, FIVEMINUTES)

async function add(msg, params) {
    if (params.length === 1) {
        const urlDecoded = params[0].split('/')
        const plateformeUrl = urlDecoded[2]
        const value = urlDecoded[3]
        const idStreams = findIDStreamsByValue(value, 'twitch')
        console.log(msg);
        if (plateformeUrl === "www.twitch.tv") { // ne s'éxécute que si la plateforme est twitch
            const result = await addTwitchFollows(value, msg)
        }
        else {
            bot.createMessage(msg.channel.id, "Plateforme non supporté ou URL invalide");
        }
    }
    else {
        bot.createMessage(msg.channel.id, "Arguments invalides");
    }
}

async function remove(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    if (params.length === 1) {
        const urlDecoded = params[0].split('/')
        const plateformeUrl = urlDecoded[2]
        const value = urlDecoded[3]
        const resultIdStreams = await findIDStreamsByValue(value, 'twitch')
        const idStreams = resultIdStreams['idStreams']
        console.log(idStreams);

        // console.log(msg);
        if (plateformeUrl === "www.twitch.tv") { // ne s'éxécute que si la plateforme est twitch
            await removeLink(idStreams, idDiscord)
            bot.createMessage(msg.channel.id, "Stream supprimmé");
        }
        else {
            bot.createMessage(msg.channel.id, "Plateforme non supporté ou URL invalide");
        }
    }
    else {
        bot.createMessage(msg.channel.id, "Arguments invalides");
    }
}

async function togglemode(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    const idDiffusion = msg['channel']['id']
    const isRegistered = await isDiscordInDB(idDiscord)
    if (!isRegistered) {
        await addDiscordinDB(idDiscord, idDiffusion)
    }
    if (await getActivationMode(idDiscord) == 1) {
        await knex(DISCORD).where('idDiscord', '=', idDiscord).update({ activation: 0 })
        bot.createMessage(msg.channel.id, "le bot s'endort ce soir");
    }
    else {
        await knex(DISCORD).where('idDiscord', '=', idDiscord).update({ activation: 1 })
        bot.createMessage(msg.channel.id, "Le bot se léve");
    }
    return
}

async function list(msg , params) {
    const idDiscord = msg['channel']['guild']['id']
    const idDiffusion = msg['channel']['id']
    const isRegistered = await isDiscordInDB(idDiscord)
    if (!isRegistered) {
        await addDiscordinDB(idDiscord, idDiffusion)
    }
    let message = "Liste des channels enregistrés"
    const result = await knex.raw(`select distinct value, plateforme from Streams, Utilise, Discord where Utilise.idDiscord = ${idDiscord} and Streams.idStreams = Utilise.idStreams order by value;`)
    const listeStreams = result[0]
    console.log(listeStreams);
    
    for (let i = 0; i < listeStreams.length; i++) {
        const element = listeStreams[i];
        if (element['plateforme'] === 'twitch') {
            message += `\n\`https://www.twitch.tv/${element['value']}\``
        }
    }
    bot.createMessage(msg.channel.id, message);
}
async function comehere(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    const idDiffusion = msg['channel']['id']
    const isRegistered = await isDiscordInDB(idDiscord)
    if (!isRegistered) {
        await addDiscordinDB(idDiscord, idDiffusion)
    }
    await knex(DISCORD).where('idDiscord', '=', idDiscord).update({ idDiffusion })
    bot.createMessage(idDiffusion, "Aussitôt dit, aussitôt fait !");
}

async function getActivationMode(idDiscord) {
    const result = await knex(DISCORD).select('activation').where('idDiscord', '=', idDiscord)
    return result[0]['activation']
}
async function removeLink(idStreams, idDiscord) {
    console.log(idStreams, idDiscord);

    await knex(UTILISE).whereRaw(`idDiscord = ${idDiscord} and idStreams = ${idStreams}`).del()
}

async function addTwitchFollows(value, msg) {
    const idDiscord = msg['channel']['guild']['id']
    let idStreams
    const isKnown = await knex(STREAMS).select('*').where('value', '=', value)
    if (isKnown.length === 0) { //si il n'est pas connu en Base de données
        const resultIsValidID = await fetch(`https://api.twitch.tv/kraken/channels/${value}`, { method: 'GET', headers })
        const isValidID = await resultIsValidID.json()
        if (isValidID['error'] === "Not Found") { // si le profil n'existe pas 
            bot.createMessage(msg.channel.id, "Cet utilisateur ne semble pas exister");
            return
        }
        else { // si il existe, alors le rajouter à la base de données
            const resultIdStreams = await knex(STREAMS).insert({ value, Plateforme: "twitch", diffuse: 0 }).returning('idStreams')
            idStreams = resultIdStreams[0]

        }
    }
    else {
        idStreams = isKnown[0]['idStreams']

    }
    const resultLink = await isAlreadyLinked(idDiscord, idStreams)
    if (resultLink) {
        bot.createMessage(msg.channel.id, "Stream **déjà** sauvegardé");
    }
    else {
        linkStreamsIDwithDiscordID(value, idDiscord, msg)
        bot.createMessage(msg.channel.id, "Stream sauvegardé");
    }
}

async function linkStreamsIDwithDiscordID(value, idDiscord, msg) {
    const idDiffusion = msg['channel']['id']
    const isKnownDiscord = await isDiscordInDB(idDiscord, idDiffusion)
    if (!isKnownDiscord) {
        await addDiscordinDB(idDiscord, idDiffusion)
    }
    const resultIdStreams = await findIDStreamsByValue(value, 'twitch')
    const idStreams = resultIdStreams['idStreams']
    await knex(UTILISE).insert({ idDiscord, idStreams })
}

async function isDiscordInDB(idDiscord) {
    const result = await knex(DISCORD).select('*').where('idDiscord', '=', idDiscord)
    if (result.length === 0) {
        return false
    }
    return true
}

async function isAlreadyLinked(idDiscord, idStreams) {

    const result = await knex(UTILISE).select('*').where('idDiscord', '=', idDiscord).andWhere('idStreams', '=', idStreams)

    if (result.length == 0) {
        return false
    }
    else {
        return true
    }
}
async function findIDStreamsByValue(value, plateforme) {
    const result = await knex(STREAMS).select('idStreams').where('value', '=', value).andWhere('plateforme', '=', plateforme)
    return result[0]
}
async function addDiscordinDB(idDiscord, idDiffusion) {
    await knex(DISCORD).insert({ idDiscord, idDiffusion, activation: 1 })
}
bot.on("ready", () => { // When the bot is ready
    console.log("Ready!"); // Log "Ready!"
    console.log(bot.channelGuildMap);
    checkDB()
});

bot.on("messageCreate", (msg) => { // When a message is created
    const message = msg.content
    // This isn't a command since it has no prefix:
    if (message.substr(0, 1) !== commandPrefix) {
        return
    }
    // Split the message into individual words:
    const parse = message.slice(1).split(' ')
    // The command name is the first (0th) one:
    const commandName = parse[0]
    // The rest (if any) are the parameters:
    const params = parse.splice(1)

    // If the command is known, let's execute it:
    if (commandName in knownCommands) {
        // Retrieve the function by its name:
        const command = knownCommands[commandName]
        // Then call the command with parameters:
        command(msg, params)
        console.log(`* Executed ${commandName} command for ${msg.author.id}`)
    } else {
        console.log(`* Unknown command ${commandName} from ${msg.author.id}`)
    }
});


bot.connect(); // Get the bot to connect to Discord

